# **NEPPO - Teste Java 2** #

João Armless se encrencou novamente. Desta vez foi com a máfia russa.

Os estimados membros dessa tão conhecida organização deram a ele apenas alguns dias de vida. Entretanto caso ele consiga entender porque este projeto nao está rodando perfeitamente, eles poupariam a sua vida.

Vamos nos esforçar para salvar a vida desse nosso amigo. Você deve configurar o projeto em sua máquina e corrigir os testes unitários, de forma que todos sejam aprovados. Os mafiosos irão validar o seu código fonte, por isso tente ser o mais objetivo possível.

Ahhh quase ia me esquencendo, não altere os testes unitários! Você deve corrigir o código já existente de forma que eles passem nos testes unitários que já estão implementados.

Crie um fork deste projeto faça as alterações necessárias e nos envie a url.

Caso tenha problemas em criar um fork, siga o link [How to Fork in bitbucket](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html#ForkingaRepository-HowtoForkaRepository)

Sugestão de IDE: [Intellij](https://www.jetbrains.com/idea/download)